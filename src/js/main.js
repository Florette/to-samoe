$(document).ready(function () {

//select
    $('.select').click(function (event) {
        if ($(this).hasClass('open')) {
            if (event.target.className == 'select__list-item') {
                console.log(event.target);
                $(this).children('.select__selected').text(event.target.innerText);
            }
            $(this).removeClass('open');
        } else {
            $(this).addClass('open');
        }
    });

//add json document + slider
    var jqxhr = $.getJSON("/js/mocha.json")
        .done(function (sliderArray) {
            // slider
            $('.slider .content-comments__quote').text(sliderArray[0].title);
            $('.slider .content-comments__name h6').text(sliderArray[0].name);
            $('.slider .content-comments__img').attr('src', sliderArray[0].avatar);
            $('.slider .content-comments__caption').text(sliderArray[0].caption);

            for (var i = 0; i < sliderArray.length; i++) {

                if (i == 0) {
                    $('.slider__points').append('<div class="slider__point active" data-id="' + i + '"></div>')
                } else {
                    $('.slider__points').append('<div class="slider__point" data-id="' + i + '"></div>');
                }

            }

            $('.slider__point').click(function () {
                var id = $(this).data('id');
                $('.slider .content-comments__quote').text(sliderArray[id].title);
                $('.slider .content-comments__name h6').text(sliderArray[id].name);
                $('.slider .content-comments__img').attr('src', sliderArray[id].avatar);
                $('.slider .content-comments__caption').text(sliderArray[id].caption);
                $('.slider__point').removeClass('active');
                $(this).addClass('active');
            })
        }).fail(function (data) {

        }).always(function (data) {

        });

//touch points (mobile)
    $('.content-title__touch').click(function () {
        if ($(this).hasClass("show")) { //Если есть класс SHOW, то его удаляем вместе c TOUCH_LINE
            $('.content-title__links-hide').animate({"opacity": "0"}, 300, function () {
                $('.content-title__links-hide').hide();
                $('.content-title__touch').removeClass("show");
                $('.content-title__touch-point').removeClass('content-title__touch-line');
            });
        } else { //Если нет класса SHOW, то его добавляем вместе c TOUCH_LINE
            $('.content-title__links-hide').show().animate({"opacity": "1"}, 300, function () {
                $('.content-title__touch').addClass("show");
            });
            $('.content-title__touch').show().animate("show", 300, function () {
                $('.content-title__touch-point').addClass('content-title__touch-line');
            });
        }
    });
});